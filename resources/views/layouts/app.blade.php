<!doctype html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <title>DocumentIMSS - Inicio</title>

        <!-- Bootstrap core CSS -->
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="{{ asset('css/bootstrap.general.css') }}" rel="stylesheet">
        <link href="{{ asset('css/fontawesome.all.min.css') }}" rel="stylesheet">

        @yield('estilos')
    </head>
    <body>
        <nav class="navbar navbar-expand-sm navbar-light bg-light rounded p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
            <a href="{{ url('/') }}" class="navbar-brand d-flex align-items-center">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                <h1 class="navbar-brand">DocumentIMSS</h1>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#prn_navbar" aria-controls="prn_navbar" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="prn_navbar">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ url('users') }}">
                            <i class="fas fa-user-friends"></i> Usuarios
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-dark" href="#">
                            <i class="fas fa-link"></i> Link
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-dark" href="#" id="prn_dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                            <i class="fas fa-bars"></i> Catalogos 
                        </a>
                        <div class="dropdown-menu" aria-labelledby="prn_dropdown">
                            <a class="dropdown-item" href="{{ url('ocuppation') }}">Catalogo de puestos</a>
                            <a class="dropdown-item" href="#">Catalogo de ...</a>
                        </div>
                    </li>
                </ul>
                <div class="btn-group">
                    <div class="data-menu">
                        <p class="data-title">{{ Auth::user()->prefix }} {{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</p>
                        <p class="data-body">{{ Auth::user()->occupation->name }}</p>
                    </div>
                    <button type="button" class="btn dropdown-toggle dropdown-toggle-split btn-border-logout" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="{{ url('password/update') }}">
                            <i class="fas fa-lock"></i>
                            Cambiar contraseña
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ url('logout') }}" 
                            onclick="event.preventDefault(); 
                            document.getElementById('logout-form').submit();">
                                <i class="fas fa-door-closed"></i>
                                Cerrar sesion
                        </a>
                        <form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                </div>
            </div>
        </nav>

        <div class="container">

            <main role="main">
                <div class="row justify-content-center">
                    <div class="col">
                        @if (Session::has('success'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <strong>{!! Session::get('success') !!}</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        @if (Session::has('error'))
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <strong>{!! Session::get('error') !!}</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                    </div>
                </div>

                @yield('content')
            </main>

            <footer class="pt-2 my-md-5 pt-md-5 border-top">
                <div class="row">
                    <div class="col-12 col-md">
                        <img class="mb-2" src="{{ asset('img/bootstrap-solid.svg') }}" alt="" width="40" height="40">
                        <small class="d-block mb-3 text-muted">&copy; 2017-2019</small>
                    </div>
                </div>
            </footer>
        </div>

        <!-- Bootstrap core JS -->
        <script type="text/javascript" src="{{ asset('js/jquery-3.3.1.slim.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/popper.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
        <!-- Custom scripts for this template -->
        <script type="text/javascript" src="{{ asset('js/bootstrap.general.js') }}"></script>
        @yield('scripts')
    </body>
</html>
