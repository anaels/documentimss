@extends('layouts.app')

@section('content')
    <div class="text-center h4 py-2">Gestión de usuarios</div>
    <a class="btn btn-outline-primary my-3 float-right" href="{{ url('user/new') }}" role="button">
        <i class="fas fa-user-plus"></i> Nuevo usuario 
    </a>
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                @foreach ($titles as $title)
                    <th scope="col">{{ $title }}</th>
                @endforeach
                </tr>
            </thead>
            <tbody>
            @foreach ($information as $data)
                <tr>
                    <th scope="row">{{ $data->id }}</th>
                    <td>{{ $data->username }}</td>
                    <td>{{ $data->prefix }} {{ $data->first_name }} {{ $data->last_name }}</td>
                    <td>{{ $data->email }}</td>
                    <td>{{ $data->occupation->name }}</td>
                    <td>
                        <a class="btn btn-outline-info" href='#' role="button" title="Actualizar datos del usuario">
                            <i class="fas fa-edit"></i>
                        </a>
                        <form action="#" method="post" style="display: inline-block;">
                            {{ method_field('delete') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-outline-danger" onclick="validar(this);" title="Eliminar usuario">
                                <i class="fas fa-trash-alt"></i>
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    {{ $information->links('layouts.pagination') }}
@endsection
