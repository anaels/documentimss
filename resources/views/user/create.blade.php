@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <div class="col-8">           
        <div class="text-center h4 py-2">Registrar nuevo usuario</div>

        <form class="needs-validation" method="POST" action="{{ url('user/new') }}" novalidate>
            {{ csrf_field() }}

            <div class="form-group">
                <label for="new_ocuppation">Nombre del puesto de trabajo</label>
                <div class="input-group">
                    <input type="text" class="form-control" id="new_ocuppation" name="new_ocuppation" placeholder="Puesto de trabajo" required>
                    @if ($errors->has('new_ocuppation'))
                        <span class="laravel-error">
                            <strong>{{ $errors->first('new_ocuppation') }}</strong>
                        </span>
                    @else
                        <div class="invalid-feedback">
                            <strong>Ingrese el puesto de trabajo.</strong>
                        </div>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <label for="email">Correo electronico</label>
                <div class="input-group">
                    <input type="text" class="form-control" id="email" name="email" placeholder="Correo electronico" required>
                    @if ($errors->has('email'))
                        <span class="laravel-error">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @else
                        <div class="invalid-feedback">
                            <strong>Ingrese el puesto de trabajo.</strong>
                        </div>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <label for="ocuppation">Puesto de trabajo</label>
                <div class="ocuppation">
                    <select id="ocuppation" name="ocuppation" class="form-control">
                        <option value='' selected>Seleccione un puesto</option>
                        @foreach ($puestos as $p)
                            <option value="{{ $p->id }}">{{ $p->name }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('ocuppation'))
                        <span class="laravel-error">
                            <strong>{{ $errors->first('ocuppation') }}</strong>
                        </span>
                    @else
                        <div class="invalid-feedback">
                            <strong>Seleccione el puesto de trabajo.</strong>
                        </div>
                    @endif
                </div>
            </div>

            <a class="btn btn-secondary" href="{{ url('ocuppation') }}" role="button">
                <i class="fas fa-angle-left"></i> Volver
            </a>
            <button type="submit" class="btn btn-primary float-right">Registrar usuario</button>
        </form>
    </div>
</div>
@endsection
