@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <div class="col-8">
        <div class="text-center h4 py-2">Actualizar datos del puesto de trabajo</div>

        <form class="needs-validation" method="POST" action="{{ url('ocuppation', $puesto->id) }}" novalidate>
            {{ csrf_field() }}
            {{ method_field('PATCH') }}

            <div class="form-group">
                <label for="ocuppation">Nombre del puesto de trabajo</label>
                <div class="input-group">
                    <input type="text" class="form-control" id="ocuppation" name="ocuppation" placeholder="Puesto de trabajo" value="{{ $puesto->name }}" required>
                    @if ($errors->has('ocuppation'))
                        <span class="laravel-error">
                            <strong>{{ $errors->first('ocuppation') }}</strong>
                        </span>
                    @else
                        <div class="invalid-feedback">
                            <strong>Ingrese el puesto de trabajo.</strong>
                        </div>
                    @endif
                </div>
            </div>

            <a class="btn btn-secondary" href="{{ url('ocuppation') }}" role="button">
                <i class="fas fa-angle-left"></i> Volver
            </a>
            <button type="submit" class="btn btn-primary float-right">Actualizar puesto</button>
        </form>
    </div>
</div>
@endsection
