@extends('layouts.app')

@section('content')
    <div class="text-center h4 py-2">Catalogo de puestos de trabajo</div>
    <a class="btn btn-outline-primary my-3 float-right" href="{{ url('ocuppation/create') }}" role="button">
        <i class="fas fa-plus"></i> Nuevo puesto 
    </a>
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                @foreach ($titles as $title)
                    <th scope="col">{{ $title }}</th>
                @endforeach
                </tr>
            </thead>
            <tbody>
            @foreach ($information as $data)
                <tr>
                    <th scope="row">{{ $data->id }}</th>
                    <td>{{ $data->name }}</td>
                    <td>
                        <a class="btn btn-outline-info" href='{{ url("ocuppation/$data->id/edit")}}' role="button" title="Actualizar puesto de trabajo">
                            <i class="fas fa-edit"></i>
                        </a>
                        <form action="{{ url('ocuppation', $data->id) }}" method="post" style="display: inline-block;">
                            {!! method_field('delete') !!}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-outline-danger" onclick="validar(this);" title="Eliminar puesto de trabajo">
                                <i class="fas fa-trash-alt"></i>
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    {{ $information->links('layouts.pagination') }}
@endsection
