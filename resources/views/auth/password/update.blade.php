@extends('layouts.app')

@section('estilos')
@endsection

@section('content')
    <div class="row">
        <div class="col border-right">
            <form class="needs-validation" method="POST" action="{{ url('/password/update') }}" novalidate>
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="old_password">Contraseña actual</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fas fa-eye"></i>
                            </span>
                        </div>
                        <input type="password" class="form-control" id="old_password" name="old_password" placeholder="Contraseña actual" required>
                        @if ($errors->has('old_password'))
                            <span class="laravel-error">
                                <strong>{{ $errors->first('old_password') }}</strong>
                            </span>
                        @else
                            <div class="invalid-feedback">
                                <strong>Ingrese la contraseña actual.</strong>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label for="new_password">Nueva contraseña</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fas fa-eye"></i>
                            </span>
                        </div>
                        <input type="password" minlength="8" class="form-control" id="new_password" name="new_password" placeholder="Nueva contraseña" required>
                        @if ($errors->has('new_password'))
                            <span class="laravel-error">
                                <strong>{{ $errors->first('new_password') }}</strong>
                            </span>
                        @else
                            <div class="invalid-feedback">
                                <strong>Ingrese la nueva contraseña con ocho caracteres minimo.</strong>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label for="new_password_confirmation">Confirme contraseña</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fas fa-eye"></i>
                            </span>
                        </div>
                        <input type="password" minlength="8" class="form-control" id="new_password_confirmation" name="new_password_confirmation" placeholder="Confirme nueva contraseña" required>
                        @if ($errors->has('new_password_confirmation'))
                            <span class="laravel-error">
                                <strong>{{ $errors->first('new_password_confirmation') }}</strong>
                            </span>
                        @else
                            <div class="invalid-feedback">
                                <strong>Ingrese la confirmacion con ocho caracteres minimo.</strong>
                            </div>
                        @endif
                    </div>
                </div>

                <button type="submit" class="btn btn-secondary btn-lg btn-block">Cambiar contraseña</button>
            </form>
        </div>
        <div class="col border-left">
            <div class="jumbotron">
                <p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $("div.input-group-prepend").click(function(){
                $(this).find('i').toggleClass("fa-eye").toggleClass("fa-eye-slash");

                var input = $(this).parent(".input-group").find('input');

                if(input.attr("type") == "text"){
                    input.attr('type', 'password');
                }else if(input.attr("type") == "password"){
                    input.attr('type', 'text');
                }
            })
        });
    </script>
@endsection
