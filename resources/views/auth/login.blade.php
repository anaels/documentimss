<!doctype html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <title>DocumentIMSS - Login</title>

        <!-- Bootstrap core CSS -->
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="{{ asset('css/bootstrap.login.css') }}" rel="stylesheet">
    </head>
    <body>
        <form class="form-signin needs-validation" method="POST" action="{{ url('/login') }}" novalidate>
            {{ csrf_field() }}
            
            <div class="text-center mb-4">
                <h1 class="h2 mb-3 font-weight-normal">Iniciar sesion</h1>
            </div>

            <div class="form-label-group {{ $errors->has('username') ? ' has-error' : '' }}">
                <input type="text" id="username" name="username" class="form-control" pattern="[0-9]{8,10}" placeholder="Matricula" value="{{ old('username') }}" required autofocus>
                <label for="lgn_matricula">Matricula</label>
                @if ($errors->has('username'))
                    <div class="laravel-error">
                        <strong>{{ $errors->first('username') }}</strong>
                    </div>
                @else
                    <div class="invalid-feedback">
                        <strong>Ingrese una matricula valida.</strong>
                    </div>
                @endif
            </div>

            <div class="form-label-group {{ $errors->has('password') ? ' has-error' : '' }}">
                <input type="password" id="password" name="password" class="form-control" placeholder="Contraseña" required>
                <label for="password">Contraseña</label>
                @if ($errors->has('username'))
                    <span class="laravel-error">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                @else
                    <div class="invalid-feedback">
                        <strong>Ingrese su contraseña.</strong>
                    </div>
                @endif
            </div>

            <div class="custom-control custom-checkbox mb-3">
                <input type="checkbox" class="custom-control-input" id="remember" name="remember" {{ old('remember') ? 'checked' : '' }}>
                <label class="custom-control-label" for="remember">Recuerdame</label>
            </div>

            <button class="btn btn-lg btn-primary btn-block" type="submit">Ingresar</button>
            <p class="mt-3 mb-1 text-muted text-center">&copy; 2017-2019</p>
        </form>

        <!-- Bootstrap core JS -->
        <script type="text/javascript" src="{{ asset('js/jquery-3.3.1.slim.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/popper.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
        <!-- Custom scripts for this template -->
        <script type="text/javascript" src="{{ asset('js/bootstrap.login.js') }}"></script>
    </body>
</html>
