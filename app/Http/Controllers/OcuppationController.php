<?php

namespace DocumentIMSS\Http\Controllers;

use Illuminate\Http\Request;
use DocumentIMSS\Ocuppation as Ocuppations;

class OcuppationController extends Controller
{
    public function index() {
        $titles = array('#', 'Puesto de trabajo', 'Acciones');
        // $data = Ocuppations::all();
        $data = Ocuppations::where('status', '=', 1)->paginate(2);
        return view('ocuppation.index')->with('titles', $titles)
                                       ->with('information', $data);
    }

    public function create() {
        return view('ocuppation.create');
    }

    public function store(Request $request) {
        $Puesto = new Ocuppations([
          'name' => $request->get('new_ocuppation'),
          'status' => 1
        ]);

        if($Puesto->save()){
            return redirect('ocuppation/create')->with('success', '¡Puesto de trabajo registrado exitosamente!');
        }else{
            return redirect('ocuppation/create')->with('error', '¡Hubo un error al registrar el puesto, intentelo mas tarte!');
        }
    }

    public function show($id) {
        //
    }

    public function edit($id) {
        // Validar que el ID ingresado sea correcto
        if(!ctype_digit($id)) {
            return redirect('ocuppation')->with('error', '¡Hubo un error al recuperar el puesto de trabajo. Identificador incorrecto!');
        }
        //Recuperar datos del puesto de trabajo
        $puesto = Ocuppations::where('status', '=', 1)->find($id);

        //Validar datos recibidos
        if(is_null($puesto)){
            return redirect('ocuppation')->with('error', '¡Hubo un error al recuperar el puesto de trabajo!');
        }

        //mostrar datos recibidos
        // var_dump($puesto);
        return view('ocuppation.update')->with('puesto', $puesto);
    }

    public function update(Request $request, $id) {
        // var_dump($id);
        // var_dump($request); 
        $puesto = Ocuppations::find($id);
        // if(is_null($puesto)){
        //     return redirect('ocuppation')->with('error', '¡Hubo un error al recuperar el puesto de trabajo!');
        // }

        $puesto->name = $request->ocuppation;

        if($puesto->save()){
            return redirect('ocuppation')->with('success','Puesto de trabajo actualizado satisfactoriamente');
        }else{
             return redirect('ocuppation')->with('error', '¡Hubo un error al actualizar el puesto de trabajo!');
        }
    }

    public function destroy($id) {
        $puesto = Ocuppations::find($id);
        if(is_null($puesto)){
            return redirect('ocuppation')->with('error', '¡Hubo un error al eliminar el puesto de trabajo!');
        } else {        
            $puesto->status = 0;
            if($puesto->save()){
                return redirect('ocuppation')->with('success', '¡El puesto de trabajo se elimino exitosamente!');
            }else{
                return redirect('ocuppation')->with('error', '¡Hubo un error al eliminar el puesto de trabajo!');
            }
        }
    }
}
