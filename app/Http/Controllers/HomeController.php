<?php

namespace DocumentIMSS\Http\Controllers;

use Auth as Session;
use Hash as Password;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        return view('home');
    }

    public function showChangePasswordForm(){
        return view('auth.password.update');
    }

    public function changePassword(Request $request){
        // Validar que la contraseña actual sea correcta
        if (!(Password::check($request->get('old_password'), Session::user()->password))) {
            return redirect()->back()->with("error","Contraseña actual incorrecta");
        }

        // Validar que la contraseña actual no sea la misma que la nueva
        if (strcmp($request->get('old_password'), $request->get('new_password')) === 0) {
            return redirect()->back()->with("error","La nueva contraseña y la actual deben ser diferentes.");
        }

        // Validar que la nueva contraseña sea confirmada
        if (strcmp($request->get('new_password'), $request->get('new_password_confirmation')) !== 0) {
            echo "La confirmacion de contraseña es incorrecta.";
            return redirect()->back()->with("error","La confirmacion de contraseña es incorrecta.");
        }

        //Reglas de validacion para los campos
        $this->validate($request, [
            'old_password' => 'required|string',
            'new_password' => 'required|string|min:8',
            'new_password_confirmation' => 'required|string|min:8',
        ],[],[
            'old_password' => 'contraseña actual',
            'new_password' => 'nueva contraseña',
            'new_password_confirmation' => 'confirmacion'
        ]);

        //Change Password
        $user = Session::user();
        $user->password = bcrypt($request->get('new_password'));
        $user->save();

        return redirect()->back()->with("success","¡Contraseña actualizada exitosamente!");
    }
}
