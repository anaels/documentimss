<?php

namespace DocumentIMSS\Http\Controllers;

use Illuminate\Http\Request;
use DocumentIMSS\User as Users;
use DocumentIMSS\Ocuppation as Ocuppations;

class UserController extends Controller
{
    public function index() {
        $titles = array('#', 'Matricula', 'Nombre', 'Correo', 'Ocupacion', 'Acciones');
        $data = Users::paginate(1);
        return view('user.index')->with('titles', $titles)
                                       ->with('information', $data);
    }

    public function showNewUserForm(){
    	$puestos = Ocuppations::where('status', '=', 1);
    	return view('user.create')->with('puestos', $puestos);
    }
    public function newUser(Request $request){
    	var_dump($request->get());
    }
}
