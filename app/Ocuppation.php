<?php

namespace DocumentIMSS;

use Illuminate\Database\Eloquent\Model;

class Ocuppation extends Model
{
    protected $fillable = ['name', 'status'];
}
