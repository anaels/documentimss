<?php

namespace DocumentIMSS;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    // public function __construct() { $user = User::with('departament')->find(Auth::id()); View::share('user', $user); }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'first_name', 'last_name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function occupation(){
        // return $this->belongsTo('DocumentIMSS/Ocuppation');
        return $this->belongsTo(Ocuppation::class, 'fk_ocuppation');
    }

}
