<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Auth::routes();
Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');

Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Route::group(['middleware' => 'auth'], function () {

//     // All my routes that needs a logged in user
// 	// Route::get('/principal', 'HomeController@index')->name('principal');

// });
Route::get('/principal', 'HomeController@index')->name('principal');
Route::get('/password/update', 'HomeController@showChangePasswordForm');
Route::post('/password/update', 'HomeController@changePassword');
// Route::group(['middleware' => ['auth']], function () {
//     Route::get('/principal', 'HomeController@index')->name('principal');
// });
// Route::group(['middleware' => ['auth']], function() {
//     Route::get('/principal', 'HomeController@index')->middleware('auth');
// });
Route::resource('ocuppation', 'OcuppationController');

Route::get('/users','UserController@index');
Route::get('/user/new', 'UserController@showNewUserForm');
Route::post('/user/new', 'UserController@newUser');